import "./App.css";
import { Component } from "react";
// import Clock from "./components/Clock";
import SecoundClock from "./components/SecoundClock";
export default class App extends Component {
  constructor() {
    super();
    console.log("App.js - constructor()");
    this.state = { time: new Date(), showClock: true, color: false };

    this.changeShowClock = this.changeShowClock.bind(this);
    this.changeColor = this.changeColor.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    console.log("App.js - getDerivedStateFromProps()");
    return state;
  }

  ticker() {
    // this.setState({ time: new Date() });
  }
  changeShowClock() {
    this.setState({ showClock: !this.state.showClock });
  }
  changeColor() {
    this.setState({ color: !this.state.color });
  }
  render() {
    console.log("App.js - Render()");
    return (
      <div className="App">
        <h3>Welcome to Clock</h3>
        <br />
        {
          this.state.showClock ? (<SecoundClock time={this.state.time} color={this.state.color} />) : null
        }
        <hr />
        <button onClick={this.changeShowClock}>showClock</button>
        <button onClick={this.changeColor}>changeColor</button>
      </div>
    );
  }

  componentDidMount() {
    console.log("App.js - ComponentDidMount()");
    this.timer = setInterval(() => this.ticker(), 1000);
  }
}
