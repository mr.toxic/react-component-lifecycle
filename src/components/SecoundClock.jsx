import { useEffect } from "react";

const SecoundClock = ({ time, color }) => {
  useEffect(() => {
    console.log("SecoundClock.jsx - useEffect() --> Mount");
    return () => {
      console.log("SecoundClock.jsx - useEffect() --> Unmount");
    };
  }, []);

  useEffect(() => {
    console.log("SecoundClock.jsx - useEffect() --> Updating color");
  }, [color]);

  const style = {
    color: color ? "tomato" : "black",
  };
  return (
    <>
      <p style={style}>Time is : {time.toLocaleTimeString()}</p>
    </>
  );
};

export default SecoundClock;
