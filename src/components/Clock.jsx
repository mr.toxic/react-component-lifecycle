import { Component } from "react";

class Clock extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    console.log("Clock.jsx - Contructor()");
  }

  static getDerivedStateFromProps(props, state) {
    console.log("Clock.jsx - GetDerivedStateFromProps()");
    return state;
  }

  shouldComponentUpdate(nextProps, nextState){
    console.log("Clock.jsx - ShouldComponentUpdate()");
    if (this.props.time !== nextProps.time) {
        return true
    }
    return false
  }

  getSnapshotBeforeUpdate(prevProps, prevState){
    console.log("Clock.jsx - getSnapshotBeforeUpdate()");
    return {
        prevProps,prevState
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot){
    console.log("Clock.jsx - ComponentDidUpdate");
  }

  render() {
    const {time, color} = this.props
    const style = {
        color: color? 'tomato': "black"
    }
    return (
      <>
        <p style={style}>Time is : {time.toLocaleTimeString()}</p>
      </>
    );
  }

  componentDidMount() {
    console.log("Clock.jsx - ComponentDidMount()");
  }
  componentWillUnmount(){
    console.log("Clock.jsx - ComponentWillUnmount()");
  }
}

export default Clock;
